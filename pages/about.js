import Head from 'next/head'
import { useRouter } from 'next/router'

import styles from '../styles/About.module.css'

export default function About() {
    const router = useRouter()

  return (
    <div className={styles.container}>
      <Head>
        <title>About Ziqx Vercel</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Amazing things happens at Ziqx <br/>
          <a onClick={() => router.push('/')}> Go to Home</a>
        </h1>

        

        <div className={styles.grid}>
          <a href="https://nextjs.org/docs" className={styles.card}>
            <h3>ZIDG &rarr;</h3>
            <p>Find in-depth information about Next.js features and API.</p>
          </a>

          <a href="https://nextjs.org/learn" className={styles.card}>
            <h3>ZINGA &rarr;</h3>
            <p>Learn about Next.js in an interactive course with quizzes!</p>
          </a>

          <a
            href="https://github.com/vercel/next.js/tree/master/examples"
            className={styles.card}
          >
            <h3>PLAZMA &rarr;</h3>
            <p>Discover and deploy boilerplate example Next.js projects.</p>
          </a>

          <a
            href="https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            className={styles.card}
          >
            <h3>MANZIL DEV &rarr;</h3>
            <p>
              Instantly deploy your Next.js site to a public URL with Vercel.
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://ziqx.in"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          Ziqx
        </a>
      </footer>
    </div>
  )
}
